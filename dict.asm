%include 'lib.inc'
%include 'colon.inc' ; все ради DICTIONARY_KEY_OFFSET
global find_word
section .text
find_word:
	cmp rsi, 0
	je .end
	.loop:
		push rdi
		push rsi
		add rsi, DICTIONARY_KEY_OFFSET
		call string_equals
		pop rsi
		pop rdi
		cmp rax, 1
		je .end

		mov rsi, [rsi]
		cmp rsi, 0
		jne .loop
	.end:
		mov rax, rsi
		ret

