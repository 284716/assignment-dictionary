outdir = output
all: lib.o dict.o main.o
	ld -o $(outdir)/main $(outdir)/main.o output/lib.o $(outdir)/dict.o
lib.o: lib.asm
	nasm -felf64 -o $(outdir)/lib.o lib.asm
dict.o: dict.asm lib.o colon.inc
	nasm -felf64 -o $(outdir)/dict.o dict.asm
main.o: main.asm dict.o lib.o words.inc colon.inc
	nasm -felf64 -o $(outdir)/main.o main.asm
clean:
	rm -rf $(outdir)/*.o
