
%include 'lib.inc'
%include 'words.inc'
%include 'dict.inc'

%define stdin 0
%define stdout 1
%define stderr 2
%define sys_read 0

%define def_newline 0xA
%define def_quotes 0x22	; "
%define def_colon 0x3A	; :
 
%define sys_write 1
%define sys_exit 60
%define MAX_LENGTH 255

section .rodata			;понтуюсь что знаю секции
str_begin: db "key '"
str_error: db "' not found in dictionary"

section .bss			;тоже понтуюсь
str_input: resb MAX_LENGTH+1	;+1 чтобы было место под последний ноль
section .text

global _start
_start: 
xor rcx, rcx
.read_loop:
	push rcx
	call read_char
	cmp rax, 0
	je .read_end
	cmp rax, def_newline
	je .read_end
	pop rcx
	mov [str_input + rcx], al
	inc rcx
	cmp rcx, MAX_LENGTH
	jb .read_loop
.read_end:
mov rdi, str_input
mov rsi, NEXT_DICTIONARY
call find_word
cmp rax, 0
je .error

push rax
mov rdi, def_quotes
call print_char		; "
mov rdi, str_input
call print_string
mov rdi, def_quotes
call print_char		; "
mov rdi, def_colon
call print_char		; :
mov rdi, def_quotes
call print_char		; "

mov rdi, str_input
call string_length
pop rdi	;тут сейчас указатель на элемент листа
add rdi, rax
inc rdi
add rdi, DICTIONARY_KEY_OFFSET
call print_string

mov rdi, def_quotes
call print_char		; "
jmp .exit

.error:
mov rdx, 5		;длина
mov rsi, str_begin	;строка
mov rax, sys_write
mov rdi, stderr
syscall
mov rdi, str_input
call string_length
mov rdx, rax		;длина
mov rsi, str_input		;строка
mov rax, sys_write
mov rdi, stderr
syscall
mov rdx, 25		;длина
mov rsi, str_error	;строка
mov rax, sys_write
mov rdi, stderr
syscall
call print_newline
.exit:
mov rax, sys_exit
syscall

